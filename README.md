# InTune Software Front End React Component Exercise

## Introduction

Your objective is to implement the missing functionality (sorting, filtering, and pagination), in the `src/DataTable.jsx` component. The UI components for specifying options in each of the three functionality types has already been provided for you. You may use the provided UI components and sample data to manually test out your implementation.

Take note of the JSDoc comment and PropTypes declarations for details about the props to be passed into the DataTable component. As an alternative, you may use React Developer Tools to inspect the props being passed into the DataTable component. 

## Getting Started

This project was scaffolded using Create React App. Running the app should be a simple as long as you have a reasonably recent version of NodeJS installed (e.g. 12).

    npm install
    npm start

## Guidelines

- Use of third party libraries is permitted but should not be necessary to finish the implementation.
- You may look up documentation and ask your interviewer for hints.
- You may use `Array.prototype.sort` for sorting purposes. There is no need to implement a sorting algorithm.