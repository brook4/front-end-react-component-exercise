import React from "react";
import PropTypes from "prop-types";

/**
 * See propTypes declaration for further details on the typings of each prop
 *
 * @param columnDef - an object defining the key field and column names of the
 * data
 * @param dataRows - an array that contains all rows of the data
 * @param pageSize - the number of rows to display at a time
 * @param pageNum - the index of the page to display (starts at 1)
 * @param sortField - the name of the field to sort by
 * @param filterFields - column name/search string key-value pairs to filter
 * rows such that only columns that contain the respective search string as a
 * substring are shown. For columns that contain numbers, convert the values
 * to strings to perform filtering.
 * @return {JSX.Element}
 */
const DataTable = ({
  columnDef,
  dataRows,
  pageSize,
  pageNum,
  sortField,
  filterFields,
}) => {
  const { columnNames, keyFieldName } = columnDef;

  return (
    <table>
      <thead>
        <tr>
          {columnNames.map((columnName) => (
            <th key={columnName}>{columnName}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {dataRows.map((row) => (
          <tr key={row[keyFieldName]}>
            {columnNames.map((columnName) => (
              <td key={columnName}>{row[columnName]}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

DataTable.propTypes = {
  columnDef: PropTypes.shape({
    keyFieldName: PropTypes.string.isRequired,
    columnNames: PropTypes.arrayOf(PropTypes.string).isRequired,
  }).isRequired,
  dataRows: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ).isRequired,
  pageSize: PropTypes.number,
  pageNum: PropTypes.number,
  sortField: PropTypes.string,
  filterFields: PropTypes.objectOf(PropTypes.string)
};

export default DataTable;
