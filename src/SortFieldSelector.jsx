import React, { Fragment } from "react";

const SortFieldSelector = ({ fieldNames, sortField, setSortField }) => {
  return (
    <fieldset>
      <legend>Select sort field</legend>
      <div>
        {fieldNames.map((fieldName) => {
          const id = `sortFieldChoice${fieldName}`;
          return (
            <Fragment key={fieldName}>
              <input
                id={id}
                type="radio"
                checked={sortField === fieldName}
                onChange={() => setSortField(fieldName)}
              />
              <label htmlFor={id}>{fieldName}</label>
            </Fragment>
          );
        })}
      </div>
      <button onClick={() => setSortField("")} disabled={sortField === ""}>
        Clear Selection
      </button>
    </fieldset>
  );
};

export default SortFieldSelector
