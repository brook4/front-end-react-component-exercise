import React, { Fragment, useEffect } from "react";

const PageSelector = ({
  pageSizes,
  pageSize,
  setPageSize,
  pageNum,
  setPageNum,
  numItems,
}) => {
  useEffect(() => {
    if ((pageNum - 1) * pageSize >= numItems) {
      setPageNum(1);
    }
  }, [pageNum, pageSize, numItems, setPageNum]);

  useEffect(() => {
    if (!pageSizes.includes(pageSize)) {
      setPageSize(pageSizes[0]);
    }
  }, [pageSizes, pageSize, setPageSize]);

  return (
    <div>
      <fieldset>
        <legend>Select page size</legend>
        <div>
          {pageSizes.map((size) => {
            const id = `pageSizeChoice${size}`;
            return (
              <Fragment key={size}>
                <input
                  type="radio"
                  id={id}
                  checked={pageSize === size}
                  onChange={() => setPageSize(size)}
                />
                <label htmlFor={id}>{size}</label>
              </Fragment>
            );
          })}
        </div>
      </fieldset>
      <div>
        Page&nbsp;
        {pageNum}
        &nbsp;of&nbsp;
        {Math.floor(numItems / pageSize) + 1}
        <button disabled={pageNum === 1} onClick={() => setPageNum(pageNum - 1)}>
          Previous
        </button>
        <button
          disabled={pageNum * pageSize >= numItems}
          onClick={() => setPageNum(pageNum + 1)}
        >
          Next
        </button>
      </div>
    </div>
  );
};

export default PageSelector;
