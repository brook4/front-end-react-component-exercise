import React from "react";

const FilterFieldInput = ({ filterFields, setFilterFields }) => {
  return (
    <ul>
      {Object.keys(filterFields).map((fieldName) => (
        <li key={fieldName}>
          <label htmlFor={fieldName}>{fieldName}</label>
          <input
            id={fieldName}
            value={filterFields[fieldName]}
            onChange={({ target }) =>
              setFilterFields({
                ...filterFields,
                [fieldName]: target.value,
              })
            }
          />
        </li>
      ))}
    </ul>
  );
};

export default FilterFieldInput;
