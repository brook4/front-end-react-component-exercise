import DataTable from "./DataTable";
import PageSelector from "./PageSelector";
import sampleData from "./sampleData.json";
import { useState } from "react";
import FilterFieldInput from "./FilterFieldInput";
import SortFieldSelector from "./SortFieldSelector";

const sampleDataColumnDef = {
  keyFieldName: "id",
  columnNames: ["id", "make", "model", "year"],
};

const initialFilterFields = {};
sampleDataColumnDef.columnNames.forEach(
  (columnName) => (initialFilterFields[columnName] = "")
);

const pageSizes = [5, 10, 20];
const DEFAULT_PAGE_SIZE = 10;

function App() {
  const [pageNum, setPageNum] = useState(1);
  const [pageSize, setPageSize] = useState(DEFAULT_PAGE_SIZE);
  const [filterFields, setFilterFields] = useState(initialFilterFields);
  const [sortField, setSortField] = useState("");

  return (
    <div>
      <h2>Pagination Options</h2>
      <PageSelector
        pageSizes={pageSizes}
        pageSize={pageSize}
        pageNum={pageNum}
        setPageNum={setPageNum}
        setPageSize={setPageSize}
        numItems={sampleData.length}
      />
      <h2>Text Filter Options</h2>
      <FilterFieldInput
        filterFields={filterFields}
        setFilterFields={setFilterFields}
      />
      <h2>Sort Options</h2>
      <SortFieldSelector
        sortField={sortField}
        setSortField={setSortField}
        fieldNames={sampleDataColumnDef.columnNames}
      />
      <h2>Data Table</h2>
      <DataTable
        columnDef={sampleDataColumnDef}
        dataRows={sampleData}
        pageSize={pageSize}
        pageNum={pageNum}
        sortField={sortField}
        filterFields={filterFields}
      />
    </div>
  );
}

export default App;
